#include <DmxReceiver.h>
#include <elapsedMillis.h>
#include <EEPROM.h>
#include <Bounce2.h>
#include <LPD8806.h>

#include "PWMChannel.h"
#include "SevenSegment.h"
#include "Buttons.h"

#include "Fixture.h"
#include "SPI.h"

const int nLEDs =100;

LPD8806 strip = LPD8806(nLEDs, LPD8806_DATA_PIN, LPD8806_CLOCK_PIN);


DmxReceiver dmx;
IntervalTimer dmxTimer;

bool dmx_activity_led = false;
elapsedMillis elapsed;

SevenSegment display;
PWMChannel channels;
Buttons buttons;

// ---------------------------------
// SIGN MAPPING
const uint32_t size_border = 48;
uint32_t border_map[size_border];
const uint32_t size_arrow = 28;
const uint32_t length_arrow = 13;
uint32_t arrow_map[size_arrow];
float arrow_pos[size_arrow];


float strip_hue[size_border];
float strip_sat[size_border];
bool strip_randomizer[size_border];
// ---------------------------------

unsigned int start_addr;

unsigned int mode = 0;
unsigned int param1 = 0;
unsigned int param2 = 0;
unsigned int param3 = 0;

HSV marquee_color_1;
HSV marquee_color_2;
HSV arrow_color;

unsigned int glitch_rate;
HSV backlight_color_1;
HSV backlight_color_2;
HSV backlight_color_3;

void dmxTimerISR(void){ 
    dmx.bufferService();
}

void readDMX(){
    /*  
        Channel / Function
        Example Mode
        Param 1 - Dimmer
        Param 2 - Beat
        Param 3 - Speed

        Marquee
        1 - Mode
        2 - Param 1
        3 - Param 2
        4 - Param 3
        5 - R1
        6 - G1
        7 - B1
        8 - R2
        9 - G2
        10 - B2

        Sign Backlight
        11 - Glitch Freq
        12 - R1
        13 - G1
        14 - B1
        15 - R2
        16 - G2
        17 - B2
        18 - R3
        19 - G3
        20 - B3
    */
    digitalWrite(DMX_DIRECTION_PIN, LOW);

    if (dmx.newFrame()){
        dmx_activity_led = !dmx_activity_led;
        digitalWrite(LED_BUILTIN, dmx_activity_led);
        mode = dmx.getDimmer(start_addr);
    
        //Marquee
        param1 = dmx.getDimmer(start_addr + 1);
        param2 = dmx.getDimmer(start_addr + 2);
        param3 = dmx.getDimmer(start_addr + 3);
    
        marquee_color_1.h = dmx.getDimmer(start_addr + 4);
        marquee_color_1.s = dmx.getDimmer(start_addr + 5);
        marquee_color_1.v = dmx.getDimmer(start_addr + 6);
    
        marquee_color_2.h = dmx.getDimmer(start_addr + 7);
        marquee_color_2.s = dmx.getDimmer(start_addr + 8);
        marquee_color_2.v = dmx.getDimmer(start_addr + 9);

        arrow_color.h = dmx.getDimmer(start_addr + 10);
        arrow_color.s = dmx.getDimmer(start_addr + 11);
        arrow_color.v = dmx.getDimmer(start_addr + 12);

        //Backlight
        glitch_rate = dmx.getDimmer(start_addr + 13);
    
        backlight_color_1.h = dmx.getDimmer(start_addr + 14);
        backlight_color_1.s = dmx.getDimmer(start_addr + 15);
        backlight_color_1.v = dmx.getDimmer(start_addr + 16);
        //Serial.println(dmx.getDimmer(start_addr + 14), DEC);
    
        backlight_color_2.h = dmx.getDimmer(start_addr + 17);
        backlight_color_2.s = dmx.getDimmer(start_addr + 18);
        backlight_color_2.v = dmx.getDimmer(start_addr + 19);
    }
}

void update_addr(){

    if(buttons.read_up()){
        start_addr++;

        if(start_addr > MAX_ADDRESS){
            start_addr = 1;
        }
        EEPROM.put(DMX_ADDRESS_EEPROM, start_addr);
    }

    if(buttons.read_down()){
        start_addr--;

        if(start_addr < 1){
            start_addr = MAX_ADDRESS;
        }
        EEPROM.put(DMX_ADDRESS_EEPROM, start_addr);
    }

}

void readStoredStartAddr(){
    EEPROM.get(DMX_ADDRESS_EEPROM, start_addr);

    if(start_addr < 1){
      start_addr = 1;
    }else if(start_addr > MAX_ADDRESS){
      start_addr := MAX_ADDRESS;
    }
}

void setup() {
    pinMode(DMX_DIRECTION_PIN, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);
    
    readStoredStartAddr();
    
    Serial.begin(9600);
    display.begin();
    dmx.begin();
    dmxTimer.begin(dmxTimerISR, 1000);
    buttons.begin();

    // SETUP MAPPING
    for(int i=0;i < size_border;i++){
      if (i < 25) border_map[i] = i;
      else border_map[i] = i+11;
    }
    
    for(int i=0;i < size_arrow;i++){
      if (i < 11) 
      {
        arrow_map[i] = i + 25;   
        if (i < 6) 
          arrow_pos[i] = scale(float(i),0.,5.,8.8,13.8);
        else 
          arrow_pos[i] = scale(float(i),6.,10.,13.,9.);
      }
      else
      {
        arrow_map[i] = i+48;
        if (i < 15) 
          arrow_pos[i] = scale(float(i), 11., 14.,6., 3.);
        else if (i < 19) 
          arrow_pos[i] = scale(float(i), 15., 18.,3., 0.);
        else if (i < 22) 
          arrow_pos[i] = scale(float(i), 19., 21.,1., 3.);
        else 
          arrow_pos[i] = scale(float(i), 22., 27.,3., 8.);   
      }
    }

    randomize_strip_color();
    
    strip.begin();
    strip.show();

}


void loop(){

    readDMX();
    update_addr();

    display.set_number(start_addr);
    display.update();

    if(mode < 32){
      // no parameters - just default beetlejuice sign
      beetlejuice_sign_mode();
    //trippy_mode();
    }
    else if (mode < 200)
    {
      beetlejuice_with_params();
            //beetlejuice_sign_mode(); 
           //trippy_mode();
    }else{
        //Addressable strip
        //put some code here

      trippy_mode();

    }

}

void beetlejuice_sign_mode() {
  float r, g, b, h,s,v; 

  float sin_var = ((sin((millis() * (0.004) * 2 * PI))))/2.0 + 0.5;
  float sin_var2 = ((sin((millis() * (0.004) * 2 * PI)+PI)))/2.0 + 0.5;
    

  for(int i=0;i < size_border;i++){

      if (strip_randomizer[i])
      {
        h = 0.;
        s = 100.;
      }
      else
      {
        h =  21.;
        s = 99.;
      }

     if (i%3 ==0)  v =  100;
     else if (i%3 == 1) v = sin_var * 100.; 
     else v = sin_var2 * 100.;  

     HSV_to_RGB(h,s,v,&r,&g,&b);
     strip.setPixelColor(border_map[i],strip.Color(r,b,g)); 
  }
  
  for(int i=0;i < size_arrow;i++){

     float sin_var2 = ((sin((millis() * (0.0008) * 2 * PI)+ (PI/length_arrow*arrow_pos[i]))))/2.0 + 0.5;
     float q = 1000000.;
     float sin_q = powf(q ,sin_var2)/q;

     s = 95;
     h = 22;
     v = clamp(scale(sin_q, 0., 1., -2.0, 1.), 0., 1.) * 100.;  

     HSV_to_RGB(h,s,v,&r,&g,&b);
     strip.setPixelColor(arrow_map[i],strip.Color(r,b,g)); 
  }

  // Backlight
  s = 99;
  h = 14;
  v = 100.; 
  HSV_to_RGB(h,s,v,&r,&g,&b);

  channels.update_channel_1(r,g,b);
  channels.update_channel_2(r,g,b);
  channels.update_channel_3(r,g,b);
    
  strip.show();
}

void beetlejuice_with_params() {
  if (param1 > 127)
  {
    randomize_strip_color();
  }
  float r, g, b, h,s,v; 

  float sin_var = ((sin((millis() * (0.0002 + 0.00005*param3) * 2 * PI))))/2.0 + 0.5;
  float sin_var2 = ((sin((millis() * (0.0002 + 0.00005*param3) * 2 * PI)+PI)))/2.0 + 0.5;
    

  for(int i=0;i < size_border;i++){

      if (i%3 ==0)  v =  100;
      else if (i%3 == 1) v = sin_var * 100.; 
      else v = sin_var2 * 100.;  

      if (strip_randomizer[i])
      {
        h = scale(marquee_color_1.h, 0, 255, 0, 360);
        s = scale(marquee_color_1.s, 0, 255, 0, 100);
        v = v* scale(marquee_color_1.v, 0, 255, 0., 1.);
      }
      else
      {
        h = scale(marquee_color_2.h, 0, 255, 0, 360);
        s = scale(marquee_color_2.s, 0, 255, 0, 100);
        v = v* scale(marquee_color_2.v, 0, 255, 0., 1.);

      }
     

     HSV_to_RGB(h,s,v,&r,&g,&b);
     strip.setPixelColor(border_map[i],strip.Color(r,b,g)); 
  }
  
  for(int i=0;i < size_arrow;i++){

     float sin_var2 = ((sin((millis() * (0.0001 + 0.00007*param2) * 2 * PI)+ (PI/length_arrow*arrow_pos[i]))))/2.0 + 0.5;
     float q = 100000000.;
     float sin_q = powf(q ,sin_var2)/q;

     h = scale(arrow_color.h, 0, 255, 0, 360);
     s = scale(arrow_color.s, 0, 255, 0, 100);
     v = clamp(scale(sin_q, 0., 1., -2.0, 1.), 0., 1.) * 100. * scale(arrow_color.v, 0, 255, 0., 1.);

     //s = 95;
     //h = 22;
     //v = clamp(scale(sin_q, 0., 1., -2.0, 1.), 0., 1.) * 100.;  

     HSV_to_RGB(h,s,v,&r,&g,&b);
     strip.setPixelColor(arrow_map[i],strip.Color(r,b,g)); 
  }

  // Backlight
  if (random(300) < glitch_rate)
  {    
    h = 0; s = 0;  v = 0; 
  }
  else if (glitch_rate == 255)
  {
    h = 0; s = 0; v = 0;
  }
  else{
    h = backlight_color_1.h;
    s = backlight_color_1.s;
    v = backlight_color_1.v; 
  }

  HSV_to_RGB(h,s,v,&r,&g,&b);
  //channels.update_channel_1(r,g,b);
  channels.update_channel_2(r,g,b);
  channels.update_channel_3(r,g,b);
    
  strip.show();
}

void trippy_mode()
{

  float r, g, b, h,s,v; 
  
  for(int i=0;i < size_border;i++){

      float sin_var = ((sin((millis() * (0.0002 + 0.000002*param3 + 0.000001*param3*i) * 2 * PI))))/2.0 + 0.5;
      float sin_var2 = ((sin((millis() * (0.0003 + 0.000003*param3 + + 0.0000008*param3*i) * 2 * PI)+PI)))/2.0 + 0.5;

      if (i%3 ==0)  v =  100;
      else if (i%3 == 1) v = sin_var * 100.; 
      else v = sin_var2 * 100.;  

      if (strip_randomizer[i])
      {
        h = ((int)(scale(marquee_color_1.h, 0, 255, 0, 360) + scale(sin_var, 0., 1, 0., scale(param1, 0., 255., 0., 360))))%360;
        s = scale(marquee_color_1.s, 0, 255, 0, 100);
        v = v* scale(marquee_color_1.v, 0, 255, 0., 1.);
      }
      else
      {
        h = ((int)(scale(marquee_color_2.h, 0, 255, 0, 360) + scale(sin_var, 0., 1, 0., scale(param2, 0., 255., 0., 360))))%360;
        s = scale(marquee_color_2.s, 0, 255, 0, 100);
        v = v* scale(marquee_color_2.v, 0, 255, 0., 1.);

      }
     

     HSV_to_RGB(h,s,v,&r,&g,&b);
     strip.setPixelColor(border_map[i],strip.Color(r,b,g)); 
  }
  
  for(int i=0;i < size_arrow;i++){

     float sin_var2 = ((sin((millis() * (0.0008) * 2 * PI)+ (PI/length_arrow*arrow_pos[i]))))/2.0 + 0.5;
     //float sin_var2 = ((sin((millis() * (0.0001 + 0.00007*param2) * 2 * PI)+ (PI/length_arrow*arrow_pos[i]))))/2.0 + 0.5;
     float q = 100.;
     float sin_q = powf(q ,sin_var2)/q;

     h = ((int)(scale(float(arrow_color.h), 0., 255., 0., 360.) + scale(sin_q, 0., 1., -1.0, 1.)*360.))%360;     
     s = scale(arrow_color.s, 0, 255, 0, 100);
     v = clamp(scale(sin_q, 0., 1., -2.0, 1.), 0., 1.) * 100. * scale(arrow_color.v, 0, 255, 0., 1.);

     //s = 95;
     //h = 22;
     //v = clamp(scale(sin_q, 0., 1., -2.0, 1.), 0., 1.) * 100.;  

     HSV_to_RGB(h,s,v,&r,&g,&b);
     strip.setPixelColor(arrow_map[i],strip.Color(r,b,g)); 
  }

  float sin_var = ((sin((millis() * (0.00009) * 2 * PI))))/2.0 + 0.5;
  h = (backlight_color_1.h + (int)(sin_var*scale(glitch_rate,0.,255.,0.,360.)))%360;
  //h = backlight_color_1.h;
  s = backlight_color_1.s;
  v = backlight_color_1.v - scale(sin_var*scale(glitch_rate, 0., 255., 0., 1.), 0., 1., 0., 100.);     
    
  
  //channels.update_channel_1(r,g,b);
  HSV_to_RGB(h,s,v,&r,&g,&b);
  channels.update_channel_2(r,g,b);

  h = (backlight_color_1.h + (int)(sin_var*scale(glitch_rate,0.,255.,0.,360.)))%360;
  s = backlight_color_1.s;
  sin_var = ((sin((millis() * (0.0001) * 2 * PI))))/2.0 + 0.5;
  
  v = backlight_color_1.v - scale(sin_var*scale(glitch_rate, 0., 255., 0., 1.), 0., 1., 0., 100.);     

  HSV_to_RGB(h,s,v,&r,&g,&b);
  channels.update_channel_3(r,g,b);

    
  strip.show();
}

void randomize_strip_color()
{
    for (int i = 0; i < size_border; ++i)
    {
      if(random(10) > 5)
      {
        strip_hue[i] = 0.;
        strip_sat[i] = 100.;
        strip_randomizer[i] = true; 
      } 
      else
      {
        strip_hue[i] =  21.;
        strip_sat[i] = 99.;
        strip_randomizer[i] = false;
      }
    }
}


float lerp(float p0, float p1, float a)
{
   return (1-a)*p0 + a*p1; 
}


float scale(float x, float min1, float max1, float min2, float max2)
{
   return (((max2 - min2) * (x - min1)) / (max1 - min1)) + min2;
}

float clamp(float x, float min1, float max1)
{
  if (x >= max1) 
    return max1;
  else if (x <= min1)  
    return min1;
  else
    return x; 
}
void HSV_to_RGB(float h, float s, float v, float *r, float *g, float *b)
{
  int i;
  
  float f, p, q, t;
  h = max(0.0, min(360.0, h));
  s = max(0.0, min(100.0, s));
  v = max(0.0, min(100.0, v));
  
  s /= 100;
  v /= 100;
  
  if(s == 0) {
    // Achromatic (grey)
    *r = *g = *b = round(v*127);
    return;
  }
 
  h /= 60; // sector 0 to 5
  i = floor(h);

  f = h - i; // factorial part of h
  p = v * (1 - s);
  q = v * (1 - s * f);
  t = v * (1 - s * (1 - f));
  switch(i) {
    case 0:
      *r = round(127*v);
      *g = round(127*t);
      *b = round(127*p);
      break;
    case 1:
      *r = round(127*q);
      *g = round(127*v);
      *b = round(127*p);
      break;
    case 2:
      *r = round(127*p);
      *g = round(127*v);
      *b = round(127*t);
      break;
    case 3:
      *r = round(127*p);
      *g = round(127*q);
      *b = round(127*v);
      break;
    case 4:
      *r = round(127*t);
      *g = round(127*p);
      *b = round(127*v);
      break;
    default: // case 5:
      *r = round(127*v);
      *g = round(127*p);
      *b = round(127*q);
    }
}

