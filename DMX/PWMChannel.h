#define R1_PIN 22
#define G1_PIN 23
#define B1_PIN 21

#define R2_PIN 10
#define G2_PIN 20
#define B2_PIN 9

#define R3_PIN 5
#define G3_PIN 6
#define B3_PIN 4

class PWMChannel  {
  public:
    void update_channel_1(unsigned int r, unsigned int g, unsigned int b);
    void update_channel_2(unsigned int r, unsigned int g, unsigned int b);
    void update_channel_3(unsigned int r, unsigned int g, unsigned int b);
};
