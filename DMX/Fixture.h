#define DMX_DIRECTION_PIN 2
#define DMX_ADDRESS_EEPROM 0
#define MAX_ADDRESS 493
#define LPD8806_CLOCK_PIN 7
#define LPD8806_DATA_PIN 8

typedef struct{
	unsigned int r;
	unsigned int g;
	unsigned int b;
} Color;

typedef struct{
  unsigned int h;
  unsigned int s;
  unsigned int v;
} HSV;
