#include <Arduino.h>
#include "PWMChannel.h"

  void PWMChannel::update_channel_1(unsigned int r, unsigned int g, unsigned int b){
    analogWrite(R1_PIN, r);
    analogWrite(G1_PIN, g);
    analogWrite(B1_PIN, b);
	}

	void PWMChannel::update_channel_2(unsigned int r, unsigned int g, unsigned int b){
    analogWrite(R2_PIN, r);
    analogWrite(G2_PIN, g);
    analogWrite(B2_PIN, b);
	}

	void PWMChannel::update_channel_3(unsigned int r, unsigned int g, unsigned int b){
    analogWrite(R3_PIN, r);
    analogWrite(G3_PIN, g);
    analogWrite(B3_PIN, b);
	}
