#include <Arduino.h>
#include <Bounce2.h>
#include "Buttons.h"
#include <elapsedMillis.h>

elapsedMillis upElapsed;
elapsedMillis downElapsed;
elapsedMillis mElapsed;

void Buttons::begin(){
  pinMode(BUTTON_M, INPUT);
  pinMode(BUTTON_UP, INPUT);
  pinMode(BUTTON_DOWN, INPUT);
	button_m = Bounce();
  button_m.attach(BUTTON_M);
  button_m.interval(INTERVAL);
  
	button_up = Bounce();
  button_up.attach(BUTTON_UP);
  button_up.interval(INTERVAL);
  
	button_down = Bounce();
  button_down.attach(BUTTON_DOWN);
  button_down.interval(INTERVAL);
}

bool Buttons::read(Bounce& button){

	bool value = false;
  button.update();
  value = button.fell();
	return value;
}

bool Buttons::read_m(){
	return read(button_m);
}
bool Buttons::read_up(){
	return read(button_up);
}

bool Buttons::read_down(){
	return read(button_down);
}
