#define SHIFT_LATCH 19
#define SHIFT_CLOCK 3
#define SHIFT_DATA 18

const unsigned int BLANK = 0b00000000;
const unsigned int DIGITS[17] = {
    0b11111100,
    0b01100000,
    0b11011010,
    0b11110010,
    0b01100110,
    0b10110110,
    0b10111110,
    0b11100000,
    0b11111110,
    0b11100110,
    0b11101110,
    0b00111110,
    0b10011100,
    0b01111010,
    0b10011110,
    0b10001110,
};

class SevenSegment  {
 public:
  void begin(void);
  void set_number(unsigned int number);
  void set_one_raw(unsigned int byte);
  void set_two_raw(unsigned int byte);
  void set_three_raw(unsigned int byte);

  void raw(unsigned int digit, unsigned int byte);
  void set_decimal(unsigned int digit, bool byte);
  void update();

 private:
   unsigned int data[3];
   unsigned int values[3];
   void digits(unsigned int number);
};
