#define BUTTON_M 17
#define BUTTON_UP 14
#define BUTTON_DOWN 12
#define INTERVAL 100

class Buttons{
  public:
	  void begin();
  	bool read_m();
  	bool read_up();
  	bool read_down();
  private:
    bool read(Bounce& button);
    Bounce button_m;
    Bounce button_up;
    Bounce button_down;
};
