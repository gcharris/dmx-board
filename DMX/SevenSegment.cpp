#include <Arduino.h>
#include "SevenSegment.h"

void SevenSegment::begin(void){
	
	data[0] = BLANK;
	data[1] = BLANK;
	data[2] = BLANK;

	pinMode(SHIFT_LATCH, OUTPUT);
	pinMode(SHIFT_CLOCK, OUTPUT);
	pinMode(SHIFT_DATA, OUTPUT);
}

void SevenSegment::update(void){
	digitalWrite(SHIFT_LATCH, LOW);
	shiftOut(SHIFT_DATA, SHIFT_CLOCK, LSBFIRST, ~data[2]);
	shiftOut(SHIFT_DATA, SHIFT_CLOCK, LSBFIRST, ~data[1]);
	shiftOut(SHIFT_DATA, SHIFT_CLOCK, LSBFIRST, ~data[0]);
	digitalWrite(SHIFT_LATCH, HIGH);
}

void SevenSegment::set_number(unsigned int number){
	digits(number);
	set_one_raw(DIGITS[values[2]]);
	set_two_raw(DIGITS[values[1]]);
	set_three_raw(DIGITS[values[0]]);
}

void SevenSegment::raw(unsigned int digit, unsigned int byte){
  data[digit] = byte;
}

void SevenSegment::set_one_raw(unsigned int byte){
	raw(0, byte);
}

void SevenSegment::set_two_raw(unsigned int byte){
	raw(1, byte);
}

void SevenSegment::set_three_raw(unsigned int byte){
	raw(2, byte);
}

void SevenSegment::digits(unsigned int number){
    values[0] = 0;
    values[1] = 0;
    values[2] = 0;
    
    unsigned int i = 0;
    
    if (number > 999){
      number = 999;
    }
    
    while(number >= 10){
        values[i] = (number % 10);
        number = number / 10;
        i++;
    }
    values[i] = number;
}
